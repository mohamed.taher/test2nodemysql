var express = require('express');
var app = express();
const { getProductsValidate, getProducts, paginationMiddleWare,
    toggleFeaturedValidate, toggleFeatured, getAllCats, } = require('./functions')


app.listen(5000, function () {
    console.log('hello with test app listening on port 5000!');
});




/*
example::
http://localhost:5000/api/products/catId/18/page/2
http://localhost:5000/api/products/catId/17/page/1
*/
app.get('/api/products/catId/:catId/page/:page', async(req, res) => {

    const { error } = getProductsValidate(req.params);
    if (error) return res.status(500).send(error.details[0].message);

    let { catId, page=1 } = req.params,
        limit = 2,
        offset = (parseInt(page) - 1) * limit;
        
    let products = await getProducts(catId, offset, limit)
    
    return res.send(products)
});



/*
example::
http://localhost:5000/api/toggleFeatured/providerId/6/productId/50
OR
http://localhost:5000/api/toggleFeatured/1
*/
app.post( ['/api/toggleFeatured/providerId/:providerId/productId/:productId',
    "/api/toggleFeatured/:id"],
    async(req, res) => {

    
    let { providerId, productId, id=false } = req.params;

    if(id) {
        const { error } = toggleFeaturedValidate(req.params);
        if (error) return res.status(500).send(error.details[0].message);
    }
    else id = !isNaN(id) ? parseInt(id) : -1

    let featured = await toggleFeatured(providerId, productId, id)

    return res.send(featured)
});



/*
example::
http://localhost:5000/api/getAllCats?page=1
OR
http://localhost:5000/api/getAllCats?page=1&pageSize=3
*/
app.get('/api/getAllCats',paginationMiddleWare(getAllCats) ,(req, res) => {
    //all magic in middleware :)
});