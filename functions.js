let connection = require('./connectdb')
var joi = require('joi')


//didn't used
let getFirstField = async (tableName, fieldName) => {
    if(!tableName || !fieldName) return 0   //marcuv rule

    let res = await connection.query(`SELECT ${fieldName} from ${tableName} LIMIT 1`)

    return res[0][fieldName]
}




var getProductsValidate = body => {
    let schema = {
        catId: joi.number().required(),
        page: joi.number().required(),
    }

    return joi.validate(body, schema)
}

let getProducts = async(catId, offset, limit) => {

    let res = await connection.query(`
        select categories.name catName, products.name productName,
        providers.name providerName, product_provider.price productPrice,
        product_provider.featured
        from
        product_provider
        JOIN products on product_provider.product_id = products.id
        JOIN providers on product_provider.provider_id = providers.id
        join categories on products.category_id = categories.id
        where
        product_provider.available = 1
        and categories.id = ${catId}
        order by
        product_provider.featured desc,
        product_provider.price
        limit ${limit}
        offset ${offset}
    `)

    return res.map(r=>({...r, featured: Boolean(Number(r.featured))}));
}




var toggleFeaturedValidate = body => {
    let schema = {
        providerId: joi.number().required(),
        productId: joi.number().required()
    }

    return joi.validate(body, schema)
}

let toggleFeatured = async (providerId, productId, id = false) => {
    let ret = {featured: false};

    let idOrProvProd = id?
    `id = ${id}`:
    `provider_id=${providerId} and product_id=${productId}`;


    let res = await connection.query(`
        update product_provider 
        set featured = 
        (1- (
            select featured
            from product_provider
            where ${idOrProvProd}
            ))
        where ${idOrProvProd}
    `);
    

    if (res)
        ret = await connection.query(`
            select featured from product_provider
            where ${idOrProvProd}
        `);

    return {featured: Boolean(ret[0].featured)};
}




let paginationMiddleWare = (apiFunction)=> {

    return async(req, res, next) => {

        let arr = await apiFunction()
    
        let { page=1, pageSize= 2 } = req.query,
            limit = pageSize,
            offset = (parseInt(page) - 1) * limit;
    
        if (!Array.isArray(arr)) return res.status(500).send("response is not array !");
        else res.send(arr.splice(offset,limit));
    }

}

let getAllCats = async() => {
    return await connection.query(`select * from categories`)
}





module.exports = {
    getProducts,
    getProductsValidate,
    getFirstField,
    toggleFeaturedValidate,
    toggleFeatured,
    getAllCats,
    paginationMiddleWare
}